# MI-MDW Selftest Sheet 

> Všichni tvrdí, jak jsou na fit-wiki špatný odpovědi na známé teoretické otázky. Nápad -- vytiskni si čistej sheet s uvedenými pouze otázkami (ty blbě nejsou), dohledej ve slajdech a vyplň správné odpovědi a tím se to nauč.
> Bonus -- vytiskneš sheet znovu a můžeš se tím tak selftestnout, jestli si odpovědi na ty otázky pamatuješ. Done.

Rendered PDF can be found in [Releases](https://gitlab.fit.cvut.cz/hlavam30/mi-mdw-selftest-sheet/tags/v1.0).

## How to build yourself

There is a `plain-questions.txt` file, containing question on each line. This can be easily converted to TeX needed format with: 

```
 $ ./convert-plain-questions.sh plain-questions.txt
```

`questions.tex` file will be created. Then you can just render the `main.tex` file however you want.